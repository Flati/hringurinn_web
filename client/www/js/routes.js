angular.module('HRingurinn.routes', ['ui.router'])

.config(function ($stateProvider, $urlRouterProvider) {


  $stateProvider
  .state('nav', {
    url: '',
    abstract: true,
    templateUrl: 'www/templates/nav.html'
  })
  .state('nav.home', {
    url: '/',
    params: { username: '' },
    templateUrl: 'www/templates/home.html',
    controller: 'homeCtrl'
  })
  .state('nav.signup', {
    url: '/signup',
    params: { name: '', email: '', fbId: undefined},
    templateUrl: 'www/templates/signup.html',
    controller: 'signupCtrl',
    pageTrack: '/signup'
  })
  .state('nav.teams', {
    url: '/teams',
    templateUrl: 'www/templates/teams.html',
    controller: 'teamsCtrl',
    pageTrack: '/teams'
  })
  .state('nav.team', {
    url: '/team/:id',
    templateUrl: 'www/templates/team.html',
    controller: 'teamCtrl',
    pageTrack: '/teamDetail'
  })
  .state('nav.account', {
    url: '/account',
    templateUrl: 'www/templates/account.html',
    controller: 'accountCtrl',
    pageTrack: '/account'
  })
  .state('nav.userControl', {
    url: '/users',
    templateUrl: 'www/templates/userControl.html',
    controller: 'userCtrl',
    pageTrack: '/users'
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/');

});
