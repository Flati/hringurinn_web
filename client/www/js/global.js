angular.module('HRingurinn').constant('global', {
  PaymentType: {
    Payed: 'payed',
    Pending: 'pending',
    None: 'none'
  }
});
