angular.module('HRingurinn.services')

.factory('userService', function($state, $rootScope) {

  var token;
  var user;

  function saveToLocal(index, object) {
    window.localStorage[index] = angular.toJson(object);
  }
  function getFromLocal(index) {
    var str = window.localStorage[index];
    if (str) {
      return angular.fromJson(str);
    } else {
      return null;
    }
  }

  function getUser() {
    if (user) return user;
    var tmp = getFromLocal("user");
    return tmp || user;
  }
  function getToken() {
    if (token) return token;
    var tmp = getFromLocal("token");
    return tmp || token;
  }

  function initialize() {
    if (!user) {
      user = getUser();
      $rootScope.user = user;
    }
    if (!token) {
      token = getToken();
    }
  }

  return {
    init: function() {
      initialize();
    },
    logout: function() {
      token = undefined;
      user = undefined;
      $rootScope.user = undefined;
      window.localStorage.clear();
      return true;
    },
    setToken: function(t) {
      saveToLocal("token", t);
      token = t;
    },
    getToken: getToken,
    setUser: function(u) {
      user = u;
      saveToLocal("user", u);
    },
    getUser: getUser,
    loggedIn: function() {
      token = getToken();
      return !!token;
    }
  };
});
