angular.module('HRingurinn.services')

.factory('authService', function(userService, $state) {

  function checkAuthorized(fromState, toState) {
    if (toState.name === "nav.home" || toState.name === "nav.signup") {
      // ignore security on home/signup
      return true;
    }
    if (userService.loggedIn()) {
      return true;
    } else {
      $state.go('nav.home');
      return false;
    }
  }

  return {
    authorized : function(fromState, toState) {
      return checkAuthorized(fromState, toState);
    },
  };
});
