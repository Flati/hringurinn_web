angular.module('HRingurinn.services')

.factory('httpService', function($http, userService) {
  var SERVER = 'http://127.0.0.1:4000';

  function sendPOSTRequest(_url, _data) {
    return $http({
      method: 'POST',
      data: _data,
      headers: {
        'Authorization' : userService.getToken()
      },
      url: _url
    });
  }

  function sendGETRequest(_url) {
    return $http.get(_url, {
      headers: {
        'Authorization': userService.getToken()
      }
    });
  }

  function sendDELETERequest(_url) {
    return $http.delete(_url, {
      headers: {
        'Authorization': userService.getToken()
      }
    });
  }

  return {
    login: function(userObj) {
      return sendPOSTRequest(SERVER + '/api/users/login', userObj);
    },
    signup: function(userObj) {
      return sendPOSTRequest(SERVER + '/api/users/signup', userObj);
    },
    getUser: function() {
      return sendGETRequest(SERVER + '/api/user');
    },
    updateUser: function(userObj) {
      return sendPOSTRequest(SERVER + '/api/users/update', userObj);
    },
    adminUpdateUser: function(userObj) {
      return sendPOSTRequest(SERVER + '/api/adminUpdate', userObj);
    },
    getUserList: function() {
      return sendGETRequest(SERVER + '/api/users');
    },
    getGames: function() {
      return sendGETRequest(SERVER + '/api/games');
    },
    addTeam: function(team) {
      return sendPOSTRequest(SERVER + '/api/teams', team);
    },
    getTeam: function(id) {
      return sendGETRequest(SERVER + '/api/teams/' + id);
    },
    deleteTeam: function(teamId) {
      return sendDELETERequest(SERVER + '/api/teams/' + teamId);
    },
    applyToTeam: function(teamId, name) {
      return sendPOSTRequest(SERVER + '/api/teams/' + teamId + '/members', { inGameName: name });
    },
    leaveTeam: function(teamId, memberId) {
      return sendDELETERequest(SERVER + '/api/teams/' + teamId + '/members/' + memberId);
    },
    setMemberStatus: function(teamId, memberId, accept) {
      return sendPOSTRequest(SERVER + '/api/teams/' + teamId + '/status', { id: memberId, accept: accept });
    },
    sendPayment: function(ssn) {
      return sendPOSTRequest(SERVER + '/api/payment', { ssn: ssn });
    },
    getVoucher: function(userID) {
      return sendPOSTRequest(SERVER + '/api/getVoucher', { id: userID });
    }
  };
});
