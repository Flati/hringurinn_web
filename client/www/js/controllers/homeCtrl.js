angular.module('HRingurinn.controllers')

.controller('homeCtrl', ['$scope', '$state', 'httpService', 'userService', '$rootScope', 'ezfb',
                function($scope, $state, httpService, userService, $rootScope, ezfb) {

  $scope.userObj = {
    username: $state.params.username,
    password: ''
  };

  $scope.login = function() {
    $scope.userObj.error = undefined;
    if (!$scope.userObj.username && !$scope.userObj.password) {
      $scope.userObj.error = 'Vinsamlegast fylltu út notandanafn og lykilorð.';
      return;
    } else if (!$scope.userObj.username) {
      $scope.userObj.error = 'Vinsamlegast fylltu út notandanafn.';
      return;
    } else if (!$scope.userObj.password) {
      $scope.userObj.error = 'Vinsamlegast fylltu út lykilorð.';
      return;
    }

    httpService.login($scope.userObj)
    .then(function (res) {
      userService.setToken(res.data.token);
      userService.setUser(res.data.user);
      $rootScope.user = res.data.user;
      $state.go('nav.teams');
    }).catch(function (err) {
      console.error(err);
      $scope.userObj.error = err.data;
    });
  };

  $scope.fbButton = function() {
    ezfb.login(function (fbRes) {
      if (fbRes.status === "connected") {
        fbLogin(fbRes);
      }
    }, {scope: 'email'});
  };

  function fbLogin(fbRes) {
    $scope.userObj.error = undefined;
    // TODO: send request to server with accessToken, expiresIn and userID
    httpService.login({
      type: "facebook",
      userId: fbRes.authResponse.userID,
      accessToken: fbRes.authResponse.accessToken,
      expiresIn: fbRes.authResponse.expiresIn
    }).then(function fbServerSuccess(hrRes) {
      // TODO: if user is incomplete, send to signup page with userID
      if (hrRes.status === 204) {
        // get relevant info and send to signup
        ezfb.api('/me?fields=id,name,email', function (res) {
          $state.go('nav.signup', { name: res.name, email: res.email, fbId: res.id });
        });
      } else {
        // user exists, log in
        userService.setToken(hrRes.data.token);
        userService.setUser(hrRes.data.user);
        $rootScope.user = hrRes.data.user;
        $state.go('nav.teams');
      }
    }).catch(function fbServerError(hrErr) {
      console.error("Error signing in to facebook", hrErr);
      $scope.userObj.error = hrErr.data;
    });
  }

  function init() {
    var user = userService.getUser();
    if (user) {
      $rootScope.user = user;
      $state.go('nav.teams');
      return;
    }
    /*
    ezfb.getLoginStatus(function (res) {
      if (res.status === "connected") {
        // already logged in
        fbLogin(res);
      }
      console.debug(res);
    });
    */
  }
  init();
}]);
