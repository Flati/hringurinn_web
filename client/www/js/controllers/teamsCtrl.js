angular.module('HRingurinn.controllers')
.controller('teamsCtrl', ['$scope', '$state', 'httpService', '$rootScope', function($scope, $state, httpService, $rootScope) {
  $scope.active = 1;
  $scope.addTeam = function() {
    if ($scope.form.game === undefined) {
      $scope.form.gameError = 'Vinsamlegast veldu leik fyrir liðið þitt.';
    } else {
      delete $scope.form.gameError;
    }
    if ($scope.form.name.length === 0) {
      $scope.form.nameError = 'Vinsamlegast skráðu nafn á liðið þitt.';
    } else {
      delete $scope.form.nameError;
    }
    if ($scope.form.name.length === 0) {
      $scope.form.inGameNameError = 'Vinsamlegast skráðu leikmanns nafnið þitt í leiknum.';
    } else {
      delete $scope.form.inGameNameError;
    }
    if (!$scope.form.nameError && !$scope.form.inGameNameError) {
      httpService.addTeam({
        name: $scope.form.name,
        game: $scope.form.game,
        inGameName: $scope.form.inGameName
      }).then(function(data) {
        $scope.active = $scope.form.game;
        $scope.form.name = '';
        getStatus();
      });
    }
  };

  function getStatus() {
    httpService.getGames().then(function(data) {
      $scope.games = data.data;
    }).catch(function (err) {
      $rootScope.logout();
      $state.go('nav.home');
    });
    httpService.getUser().then(function(data) {
      $scope.user = data.data;
    }).catch(function (err) {
      $rootScope.logout();
      $state.go('nav.home');
    });
  }
  getStatus();

  $scope.form = {
    game: undefined,
    name: '',
    inGameName: ''
  };
}]);
