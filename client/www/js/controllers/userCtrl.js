angular.module('HRingurinn.controllers')

.controller('userCtrl', ['$scope', '$state', 'httpService', 'global', '$uibModal', '$rootScope', 'toastr',
  function($scope, $state, httpService, global, $uibModal, $rootScope, toastr) {

  $scope.users = [];
  $scope.userSearch = '';
  $scope.paymentFields = [
    { name: 'Ógreitt', paymentStatus: global.PaymentType.None },
    { name: 'Pending', paymentStatus: global.PaymentType.Pending },
    { name: 'Greitt', paymentStatus: global.PaymentType.Payed },
  ];

  httpService.getUserList()
  .then(function (res) {
    $scope.users = res.data;
  }).catch(function (err) {
    $rootScope.logout();
    $state.go('nav.home');
  });

  function printVoucher(userID) {
    httpService.getVoucher(userID)
    .then(function(res) {
      console.log(res.data);
      var w = window.open("", "", "Benni var hér!");
      w.document.write(res.data);
      w.print();
      w.close();
    }).catch(function(err) {
      console.error(err);
    });
  }

  $scope.userDetails = function(userObj) {
    $uibModal.open({
      templateUrl: 'userDetailsModal.html',
      controller: 'userDetailsCtrl',
      resolve: {
        user: function() {
          return userObj;
        }
      },
      scope: $scope
    }).result.then(function (obj) {
      if (obj.form) {
        httpService.adminUpdateUser(obj.form)
        .then(function (res) {
          console.log(res.data);
          if (obj.print) {
            printVoucher(obj.form.id);
          }
          for (var i = 0; i < $scope.users.length; i++) {
            var target = $scope.users[i];
            if (target.id == obj.form.id) {
              if (target.payed !== obj.form.payment) {
                target.payed = obj.form.payment;
                toastr.success('Greiðslustaða: ' + target.payed, 'Tókst að uppfæra greiðslustöðu notanda');
              }
              if (target.role !== obj.form.role) {
                target.role = obj.form.role;
                toastr.success('Staða: ' + target.role, 'Tókst að uppfæra stöðu notanda');
              }
              break;
            }
          }
        }).catch(function (err) {
          toastr.error('Tókst ekki að uppfæra notanda', 'Error');
          console.error(err);
        });
      } else if (obj.print) {
        printVoucher(obj.fuckit);
      }
    });
  };
}])

.controller('userDetailsCtrl', ['$scope', '$state', 'httpService', '$uibModal', '$uibModalInstance', '$rootScope', 'global', 'user',
  function($scope, $state, httpService, $uibModal, $uibModalInstance, $rootScope, global, user) {
    $scope.user = user;
    $scope.underage = checkAge(user.ssn);
    $scope.form = {
      id: user.id,
      payment: user.payed,
      role: user.role,
    };
    getForm();

    /*
      Disclamer: this needs to be redone, just not right now.......
    */
    function checkAge(ssn) {
      // returns true if underage, false otherwise
      var yearBorn = parseInt(ssn[4] + ssn[5]);
      var d = new Date();
      var currentYear = parseInt(d.getFullYear().toString().substr(2,2));
      var age = 0;
      if (yearBorn < currentYear) {
        age = currentYear - yearBorn; // born 2000 or later
      } else {
        age = currentYear + (100-yearBorn);
      }
      if (age < 18) {
        return true;
      } else if (age > 18) {
        return false;
      } else {
        var currentMonth = d.getMonth(),
            currentDate  = d.getDate(),
            monthBorn    = parseInt(ssn[2] + ssn[3]),
            dateBorn     = parseInt(ssn[0] + ssn[1]);
        if (currentMonth < monthBorn) {
          return true;
        } else if (currentMonth > monthBorn) {
          return false;
        } else {
          return currentDate < dateBorn;
        }
      }
    }
    function getForm() {
      console.log("getForm", $scope.underage);
      if ($scope.underage) {
        if (user.Attendance) {
          $scope.form = {
            id: user.id,
            payment: user.payed,
            role: user.role,
            parentName: user.Attendance.parentName || undefined,
            parentSSN: user.Attendance.parentSSN || undefined,
            parentPhone: user.Attendance.parentPhone || undefined
          };
        } else {
          $scope.form = {
            id: user.id,
            payment: user.payed,
            role: user.role,
            parentName: "",
            parentSSN: "",
            parentPhone: ""
          };
        }
      } else {
        $scope.form = {
          id: user.id,
          payment: user.payed,
          role: user.role,
        };
      }
    }

    $scope.roles = [
      { name: 'Leik-admin', value: 'admin' },
      { name: 'Stjórn', value: 'council' },
      { name: 'Sjoppu-staff', value: 'shop' },
      { name: 'Checkin', value: 'checkin' },
      { name: 'Almenningur', value: 'public' }
    ];
    $scope.payments = [
      { name: $rootScope.tooltip(global.PaymentType.None), value: global.PaymentType.None },
      { name: $rootScope.tooltip(global.PaymentType.Pending), value: global.PaymentType.Pending },
      { name: $rootScope.tooltip(global.PaymentType.Payed), value: global.PaymentType.Payed }
    ];
    $scope.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };
    $scope.save = function() {
      console.log($scope.form);
      if ($scope.form.payment === $scope.user.payed &&
          $scope.form.role === $scope.user.role &&
          $scope.form.parentName === undefined &&
          $scope.form.parentSSN === undefined &&
          $scope.form.parentPhone === undefined) {
        $uibModalInstance.dismiss('cancel');
      } else {
        $uibModalInstance.close({form: $scope.form, print: false});
      }
    };
    $scope.saveAndPrint = function() {
      if ($scope.form.payment === $scope.user.payed &&
          $scope.form.role === $scope.user.role &&
          $scope.form.parentName === undefined &&
          $scope.form.parentSSN === undefined &&
          $scope.form.parentPhone === undefined) {
        $uibModalInstance.dismiss('cancel');
      } else {
        $uibModalInstance.close({form: $scope.form, print: true});
      }
    };
    $scope.printVoucher = function() {
      $uibModalInstance.close({form: undefined, print: true, fuckit: user.id});
    };
}]);
