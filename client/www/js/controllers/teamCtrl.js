angular.module('HRingurinn.controllers')

.controller('teamCtrl', ['$scope', '$state', 'httpService', '$uibModal', '$rootScope', function($scope, $state, httpService, $uibModal, $rootScope) {
  var teamId = $state.params.id;
  $scope.team = {};

  function getTeam() {
    httpService.getTeam(teamId)
    .then(function (res) {
      $scope.team = res.data;
    }).catch(function (err) {
      $rootScope.logout();
      $state.go('nav.home');
    });
  }
  function getUser() {
    httpService.getUser().then(function(data) {
      $scope.user = data.data;
    }).catch(function (err) {
      $rootScope.logout();
      $state.go('nav.home');
    });
  }

  getTeam();
  getUser();

  $scope.open = function () {
    $uibModal.open({
      templateUrl: 'myModalContent.html',
      controller: 'applicationController'
    }).result.then(function (name) {
      httpService.applyToTeam(teamId, name)
      .then(function (res) {
        getUser();
        getTeam();
      }).catch(function (err) {
        console.error(err);
      });
    });
  };

  $scope.leave = function() {
    httpService.leaveTeam($scope.user.Member.TeamId, $scope.user.Member.id)
    .then(function (res) {
      delete $scope.user.Member;
      getTeam();
    });
  };

  $scope.deleteTeam = function() {
    httpService.deleteTeam($scope.user.Member.TeamId)
    .then(function (res) {
      $state.go('nav.teams');
    });
  };

  $scope.accept = function(memberId) {
    httpService.setMemberStatus($scope.user.Member.TeamId, memberId, true).then(function () {
      getTeam();
    });
  };

  $scope.reject = function(memberId) {
    httpService.setMemberStatus($scope.user.Member.TeamId, memberId, false).then(function () {
      getTeam();
    });
  };
}]).controller('applicationController', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
  $scope.application = {
    name: ''
  };
  $scope.ok = function() {
    if ($scope.application.name.length > 0) {
      $uibModalInstance.close($scope.application.name);
    } else {
      $scope.error = 'Vinsamlegast skráðu leikmanns nafnið þitt í leiknum.';
    }
  };
  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
}]);
