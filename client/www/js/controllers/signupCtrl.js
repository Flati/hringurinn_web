angular.module('HRingurinn.controllers')

.controller('signupCtrl', function($scope, $state, httpService) {

  $scope.newUser = {
    name: $state.params.name,
    ssn: "",
    email: $state.params.email,
    phoneNumber: "",
    username: "",
    password: "",
    facebookId: $state.params.fbId
  };
  $scope.passwordCheck = "";

  $scope.signup = function() {
    if ($scope.passwordCheck != $scope.newUser.password) {
      $scope.newUser.passwordError = 'Lykilorð stemmir ekki.';
      return;
    } else {
      delete $scope.newUser.passwordError;
    }
    if (!$scope.newUser.email || !$scope.newUser.password) {
      return;
    }
    if (!validSSN($scope.newUser.ssn)) {
      $scope.newUser.ssnError = 'Kennitala er ógild.';
      return;
    } else {
      delete $scope.newUser.ssnError;
    }
    httpService.signup($scope.newUser)
    .then(function loginSuccess(res) {
      $state.go("nav.home", {username: $scope.newUser.username});
    }).catch(function loginErr(err) {
      console.error(err);
      $scope.newUser.error = err.data;
    });
  };

  function validSSN(ssn) {
    ssn = ssn.split('-').join('');
    if (ssn.length != 10) {
      return false;
    } else {
      if (isNaN(ssn)) {
        return false;
      } else {
        return ssnCheck(parseInt(ssn));
      }
    }
  }

  function ssnCheck(kt) {
    if (((kt / 1000000) % 100) < 1 || ((kt / 100000000) % 100) < 1)
        return false;
    var checker = Math.floor((kt /= 10) % 10);
    var result = 0;
    var ints = [2, 3, 4, 5, 6, 7, 2, 3];
    for (var i = 0; i < ints.length; i++) {
      result += Math.floor((kt /= 10) % 10) * ints[i];
    }
    var sumTemp = 0;
    if (result % 11 > 0)
        sumTemp = Math.floor((result / 11) + 1);
    else
        sumTemp = Math.floor(result / 11);
    sumTemp = (sumTemp * 11) - result;
    return sumTemp == checker;
  }

  function init() {
    if ($state.params.fbId) {
      $scope.showMessage = true;
    }
  }
  init();
});
