angular.module('HRingurinn.controllers')

.controller('accountCtrl', ['$scope', '$rootScope', 'httpService', 'userService', function($scope, $rootScope, httpService, userService) {

  $scope.user = angular.copy($rootScope.user);

  $scope.payer = {
    ssn: ''
  };
  $scope.editAccount = function() {
    var data = {
      name: $scope.user.name,
      phone: $scope.user.phone,
      email: $scope.user.email
    };
    httpService.updateUser(data).then(function(data) {
      $rootScope.user.name = $scope.user.name = data.data.name;
      $rootScope.user.email = $scope.user.email = data.data.email;
      $rootScope.user.phone = $scope.user.phone = data.data.phone;
      userService.setUser($scope.user);
    });
  };
  $scope.sendPayment = function() {
    $scope.user.payment = 1;
    httpService.sendPayment($scope.payer.ssn).then(function(data) {
      $rootScope.user.payed = $scope.user.payed = $rootScope.global.PaymentType.Pending;
    });
  };
}]);
