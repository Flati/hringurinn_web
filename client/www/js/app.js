angular.module('HRingurinn', ['HRingurinn.controllers', 'HRingurinn.routes', 'HRingurinn.services',
                              'ui.bootstrap', 'ezfb', 'toastr'])

.run(function($rootScope, userService, httpService, global, authService) {
  userService.init(); // get cached things if needed
  httpService.getUser().then(function(user) {
    userService.setUser(user.data);
    $rootScope.user = user.data;
  });
  $rootScope.logout = userService.logout;
  $rootScope.global = global;
  $rootScope.tooltip = function(payed) {
    payed = payed || $rootScope.user.payed;
    switch (payed) {
      case global.PaymentType.Payed:
        return 'Greitt';
      case global.PaymentType.Pending:
        return 'Óstaðfest';
      case global.PaymentType.None:
        return 'Ógreitt';
    }
  };
  $rootScope.paymentClass = function(payed) {
    payed = payed || $rootScope.user.payed;
    switch (payed) {
      case global.PaymentType.Payed:
        return 'background-success';
      case global.PaymentType.Pending:
        return 'background-warning';
      case global.PaymentType.None:
        return 'background-danger';
    }
  };
  $rootScope.paymentTextClass = function(payed) {
    payed = payed || $rootScope.user.payed;
    switch (payed) {
      case global.PaymentType.Payed:
        return 'text-success';
      case global.PaymentType.Pending:
        return 'text-warning';
      case global.PaymentType.None:
        return 'text-danger';
    }
  };
  $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams, options){
      if (authService.authorized(fromState, toState)) {
        return;
      } else {
        // toastr: unauthorized
        event.preventDefault();
      }
  });
})
.config(function(ezfbProvider, toastrConfig) {
  angular.extend(toastrConfig, {
    newestOnTop: true,
    positionClass: 'toast-bottom-right',
  });
  //AnalyticsProvider.setAccount('UA-79521132-1');
  // Change the default page event name.
  // Helpful when using ui-router, which fires $stateChangeSuccess instead of $routeChangeSuccess.
  //AnalyticsProvider.setPageEvent('$stateChangeSuccess');
  ezfbProvider.setInitParams({
    appId: '1582109195419911',
    // https://developers.facebook.com/docs/javascript/reference/FB.init
    version: 'v2.6'
  });
});
angular.module('HRingurinn.controllers', []);
angular.module('HRingurinn.services', []);
