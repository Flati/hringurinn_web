var gulp = require('gulp'),
  connect = require('gulp-connect');

gulp.task('webserver', function() {
  connect.server({
    root: './',
    port: 3000,
    livereload: true
  });
});

gulp.task('files', function() {
  gulp.src(['./www/**/*'])
  .pipe(connect.reload());
});

gulp.task('watch', function () {
  gulp.watch(['./www/**/*'], ['files']);
});

gulp.task('default', ['webserver', 'watch']);
