var end;
$(function() {
  var target = '#timer';
  InitTimer(target);

  setInterval(function() {
    var time = CalculateTimer(new Date());
    UpdateTimer(time);
  }, 50);
});

function InitTimer(target) {
  end = new Date('2017-06-17T17:06:17');
  $(target)
    .append('<div class="col-md-6 col-lg-3"><h2><span class="days"></span> daga</h2></div>')
    .append('<div class="col-md-6 col-lg-3"><h2><span class="hours"></span> klukkustundir</h2></div>')
    .append('<div class="col-md-6 col-lg-3"><h2><span class="minutes"></span> mínútur</h2></div>')
    .append('<div class="col-md-6 col-lg-3"><h2><span class="seconds"></span> sekúndur</h2></div>');
}

function CalculateTimer(now) {
  var diffTime = end.getTime() - now.getTime();
  var result = {
    seconds: Math.floor(diffTime / 1000) % 60,
    minutes: Math.floor(diffTime / 60000) % 60,
    hours: Math.floor(diffTime / 3600000) % 24,
    days: Math.floor(diffTime / 86400000)
  };
  return result;
}

function UpdateTimer(d, target) {
  $('.days', target).text(d.days);
  $('.hours', target).text(d.hours);
  $('.minutes', target).text(d.minutes);
  $('.seconds', target).text(d.seconds);
}
