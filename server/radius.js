const radius       = require('radius');
const dgram        = require('dgram');
const bcrypt       = require('bcryptjs');
const dal          = require('./dal');

function RadiusServer(settings) {
    this.config = settings || {};
    this.port = this.config.port || 1645;
    this.secret = this.config.secret || "";
    this.server = null;

    this.ACCESS_REQUEST = 'Access-Request';
    this.ACCESS_DENIED = 'Access-Reject';
    this.ACCESS_ACCEPT = 'Access-Accept';
}
RadiusServer.prototype.start = function () {
    var self = this;
    // create the UDP server
    self.server = dgram.createSocket("udp4");
     
    self.server.on('message', function (msg, rinfo) {
        if (msg && rinfo) {
            // decode the radius packet
            let packet;
            try {
                packet = radius.decode({ packet: msg, secret: self.secret });
            }
            catch (err) {
                console.log('Unable to decode packet.');
                return;
            }
   
            // if we have an access request, then
            if (packet && packet.code == self.ACCESS_REQUEST) {
		console.log(packet.attributes);
                 
                // get user/password from attributes
                const username = packet.attributes['User-Name'];
                const password = packet.attributes['User-Password'];
		const ip = packet.attributes['Framed-IP-Address'];
		if (!username || !password) {
			console.log('either no username or no password in request');
			return;
		}
 
                // verify credentials, make calls to 3rd party services, then set RADIUS response
                let responseCode = self.ACCESS_DENIED;

                dal.User.radiusRequest(username.toLowerCase()).then((user) => {
                    console.log('DEBUG: dal.User.radiusRequest response', user);
                    if(user && bcrypt.compareSync(password, user.passwordHash) && user.Attendances && user.Attendances.length === 1 && user.Attendances[0].payed === true) {
                        responseCode = self.ACCESS_ACCEPT;
			if (ip) {
				dal.User.setLanIp(user.id, ip)
				.catch((err) => { console.log(err); });
			}
                    } else {
                        responseCode = self.ACCESS_DENIED;
                    }
                    console.log('Access-Request for "' + username + '" (' + responseCode + ').');
                     
                    // build the radius response
                    const response = radius.encode_response({
                        packet: packet,
                        code: responseCode,
                        secret: self.secret
                    });
     
                    // send the radius response
                    self.server.send(response, 0, response.length, rinfo.port, rinfo.address, function (err, bytes) {
                        if (err) {
                            console.log('Error sending response to ', rinfo);
                            console.log(err);
                        }
                    });
                })
                .catch((err) => {
                    console.log('dal.User.radiusRequest catch', err);
                    const response = radius.encode_response({
                        packet: packet,
                        code: responseCode,
                        secret: self.secret
                    });
     
                    // send the radius response
                    self.server.send(response, 0, response.length, rinfo.port, rinfo.address, function (err, bytes) {
                        if (err) {
                            console.log('Error sending response to ', rinfo);
                            console.log(err);
                        }
                    });
                });
            }
        }
    });
     
    self.server.on('listening', function () {
        const address = self.server.address();
        console.log('Radius server listening on port ' + address.port);
    });
     
    self.server.bind(self.port);
};

module.exports = RadiusServer;
