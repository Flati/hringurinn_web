'use strict';
const express      = require('express');
const server       = express();
const app          = express.Router();
const jwt          = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config       = require('./config'); // get our config file
const bcrypt       = require('bcryptjs');
const dal          = require('./dal');
const utils = require('./utils');
const uuidv4 = require('uuid/v4');

app.post('/login', (req, res) => {
  // TODO: signup if not already user
  const body = req.body;

  // Validate
  if(body.type === 'facebook' || body.type === 'google') {
    if(!body.userId || !body.accessToken || !body.expiresIn) {
      return res.status(400).send('PreconditionFailedError');
    }
  } else if (!body.username || !body.password) {
    return res.status(412).send('PreconditionFailedError');
  }

  if(body.type === 'facebook') {
    dal.User.getByFacebookId(body.userId).then((user) => {
      if(!user) {
        // signup
        console.log("Facebook user not found. User needs to create user");
        return res.status(204).send("Facebook notandi fannst ekki.");
      }
      return res.status(200).json({
        user: {
          name: user.name,
          username: user.username,
          email: user.email,
          ssn: user.ssn,
          phone: user.phoneNumber,
          payed: user.payed,
          role: user.role,
        },
        token: jwt.sign({
          username: user.username,
          userId: user.id,
        }, config.jwtSecret, {
            expiresIn: '24h' // expires in 24 hours
        }),
      });
    })
    .catch((err) => {
      console.log('Fb login failed', err);
      return res.status(401).send("Facebook innskráning mistókst.");
    });
  } else {
    dal.User.getByUsername(body.username.toLowerCase()).then((user) => {
      if(user && bcrypt.compareSync(body.password, user.passwordHash)) {
        return res.status(200).json({
          user: {
            name: user.name,
            username: user.username,
            email: user.email,
            ssn: user.ssn,
            phone: user.phoneNumber,
            payed: user.payed,
            role: user.role,
          },
          token: jwt.sign({
            username: user.username,
            userId: user.id,
          }, config.jwtSecret, {
              expiresIn: '24h' // expires in 24 hours
          }),
        });
      } else {
        return res.status(401).send("Notandanafn eða lykilorð fannst ekki.");
      }
    })
    .catch((err) => {
      console.log('User/Pass login failed', err);
      return res.status(401).send("Notandanafn eða lykilorð fannst ekki.");
    });
  }
});

app.post('/signup', (req, res) => {
  const model = req.body;
  if (!model.name ||
      !model.email ||
      !model.password ||
      !model.username ||
      !model.ssn ||
      !model.phoneNumber) {
    return res.status(412).send('Vinsamlegast fylltu út öll svæði.');
  }
  // TODO: more validation
  model.username = model.username.trim().toLowerCase();
  model.email = model.email.trim().toLowerCase();

  signupUser(model).then((result) => {
    return res.status(201).json(result);
  })
  .catch((err) => {
    if(err.name === 'SequelizeUniqueConstraintError') {
      return res.status(409).send('Notandi er þegar til.');
    }
    console.log('Signup failed', err);
    return res.status(500).send('Nýskráning mistókst.');
  });
});

function signupUser(model) {
  model.passwordHash = bcrypt.hashSync(model.password, 8);
  model.role = 'public';

  return new Promise(function(resolve, reject) {
    dal.User.create(model)
    .then((user) => {
      resolve({
        userId: user.id,
        username: user.username
      });
    })
    .catch((err) => {
      console.log('Creating user failed', err);
      reject(err);
    });
  });
}

app.post('/reset_password', (req, res) => {
  const model = req.body;
  if (!model.username) {
    return res.sendStatus(412);
  }
  dal.User.getByUsername(model.username.toLowerCase()).then((user) => {
    let uuid = uuidv4();
    var expiry = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    return dal.ResetPassword.create({ expires: expiry, uuid: uuid }, user)
    .then(() => {
      return utils.Mailer.send(user.email, 'Týnt lykilorð', '<h1>Þú hefur týnt lykilorði þínu fyrir notandann ' + user.username + ' og hefur beðið um að endurstilla það.</h1><p>Til þess að endurstilla lykilorðið þarft þú að <a href="https://hringurinn.net/reset_password/' + uuid + '">smella hér</a>.</p><p>Þessi linkur verður óvirkur eftir 24 klst..</p><p>Ef þú kannast ekki við að hafa beðið um þennan póst, hafðu samband við <a href="mailto:info@tviund.com">Tvíund</a> til að kanna málið.</p><br /><br /><h3>Við hlökkum til að sjá þig í ágúst.</h3>').then(() => {
        return res.sendStatus(201);
      });
    })
    .catch((err) => {
      console.log(err);
      return res.sendStatus(400);
    });
  })
  .catch((err) => {
    console.error(err);
    return res.status(404).send('User not found.');
  });
});

app.post('/reset_password/:uuid', (req, res) => {
  if (!req.params.uuid) {
    return res.status(402).send('ID required.');
  }
  const model = req.body;
  if (!model.password) {
    return res.status(402).send('Password required.');
  }
  dal.ResetPassword.get(req.params.uuid).then((resetPassword) => {
    if (!resetPassword.active) {
      return res.status(403).send('Password already reset.');
    } else if (resetPassword.expires < new Date()) {
      return res.status(403).send('Link expired.');
    }
    const passwordHash = bcrypt.hashSync(model.password, 8);
    return dal.User.updatePassword(passwordHash, resetPassword.UserId)
    .then(() => {
      return dal.ResetPassword.deactivate(resetPassword.uuid).then(() => {
        return res.sendStatus(204);
      });
    })
    .catch((e) => {
      console.log(e);
      return res.send(e).status(400);
    });
  })
  .catch((err) => {
    console.error(err);
    return res.status(404).send('Request not found.');
  });
});

app.post('/lost_username', (req, res) => {
  const model = req.body;
  if (!model.email) {
    return res.status(402).send('Email required.');
  }
  dal.User.getByEmail(model.email.toLowerCase()).then((users) => {
    if (users.length > 0) {
      let usernames = '';
      for (var i = 0; i < users.length; i++) {
        usernames += '<br />' + users[i].username;
      }
      return utils.Mailer.send(model.email, 'Týnt notandanafn', '<h1>Þú hefur týnt notandanafninu þínu og hefur beðið um notandann sem tengist við þetta netfang.</h1><p>Notandi sem er tengdur við þetta netfang:' + usernames + '</p><p>Ef þú hefur einnig týnt lykilorðinu getur þú endurstillt það <a href="https://hringurinn.net/reset_password">hér</a>.</p><br /><br /><h3>Við hlökkum til að sjá þig í ágúst.</h3>').then(() => {
        return res.sendStatus(204);
      });
    } else {
      return res.status(404).send('Email not found.');
    }
  });
});

module.exports = app;
