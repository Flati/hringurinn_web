'use strict';
const db = require('./models');
const Op = db.Sequelize.Op;
const dal = {
  Attendance: {},
  Game: {},
  Member: {},
  News: {},
  Team: {},
  Tournament: {},
  User: {},
  Voucher: {},
  ResetPassword: {}
};

// Region: Attendance

dal.Attendance.create = (attendance) => {
  return db.Attendance.create({
    payed: attendance.payed,
    payerSSN: attendance.payerSSN,
    voucher: attendance.voucher,
    parentName: attendance.payerName,
    parentSSN: attendance.payerSSN,
    parentPhone: attendance.payerPhone,
    amount: attendance.amount,
    UserId: attendance.UserId,
    TournamentId: attendance.TournamentId,
  });
};

dal.Attendance.get = (id) => {
  return db.Attendance.findOne({
    where: {
      id: id
    }
  }).then((data) => {
    if (data) {
      return data.get({ plain: true });
    } else {
      return null;
    }
  });
};

dal.Attendance.getByUserIdAndTournamentId = (userId, tournamentId) => {
  return db.Attendance.findOne({
    where: {
      UserId: userId,
      TournamentId: tournamentId
    }
  }).then((data) => {
    if (data) {
      return data.get({ plain: true });
    } else {
      return null;
    }
  });
}

dal.Attendance.update = (attendance) => {
  return db.Attendance.update({
    payed: attendance.payed,
    payerSSN: attendance.payerSSN,
    voucher: attendance.voucher
  }, {
    where: {
      id: attendance.id
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });
};

dal.Attendance.updatePayed = (attendance) => {
  return db.Attendance.update({
    payed: attendance.payed,
    parentName: attendance.parentName,
    parentSSN: attendance.parentSSN,
    parentPhone: attendance.parentPhone,
    voucher: attendance.voucher,
    amount: attendance.amount
  }, {
    where: {
      id: attendance.id
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });
};

dal.Attendance.setVoucher = (attendance) => {
  return db.Attendance.update({
    voucher: attendance.voucher
  }, {
    where: {
      id: attendance.id
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });
};

dal.Attendance.remove = (id) => {
  return db.Attendance.destroy({
    where: {
      id: id
    }
  });
};

// Region: Game

dal.Game.create = (game) => {
  return db.Game.create({
    name: game.name,
    description: game.description,
    twitchUrl: game.twitchUrl,
    active: game.active,
    size: game.size
  });
};

dal.Game.update = (game) => {
  return db.Game.update({
    name: game.name,
    description: game.description,
    twitchUrl: game.twitchUrl,
    active: game.active,
    size: game.size
  }, {
    where: {
      id: game.id
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });
};

dal.Game.get = (id) => {
  return db.Game.findOne({
    where: {
      id: id
    }
  }).then((data) => {
    if (data)
      return data.get({ plain: true });
  });
};

dal.Game.getAll = () => {
  return db.Game.findAll({
    where: {
      active: true
    }
  }).then((data) => {
    var result = [];
    for (var i = 0; i < data.length; i++) {
      result.push(data[i].get({ plain: true }));
    }
    return result;
  });
};

dal.Game.getAllByTournamentId = (tournamentId) => {
  return db.Game.findAll({
    where: {
      active: true
    },
    include: [{
      model: db.Team,
      include: [
        {
          model: db.Member
        },
        {
          model: db.Tournament,
          where: {
            id: tournamentId
          }
        }
      ]
    }]
  }).then((data) => {
    var result = [];
    for (var i = 0; i < data.length; i++) {
      result.push(data[i].get({ plain: true }));
    }
    return result;
  });
}

// Region: Member

dal.Member.create = (member) => {
  return db.Member.create({
    status: member.status,
    inGameName: member.inGameName,
    TeamId: member.TeamId,
    UserId: member.UserId
  });
};

dal.Member.update = (member) => {
  var updateReq = {
    status: member.status,
    inGameName: member.inGameName
  };

  if(!updateReq.status)
    delete updateReq['status'];

  return db.Member.update(updateReq, {
    where: {
      id: member.id
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });
};

dal.Member.setStatus = (member) => {
  return db.Member.update({
    status: member.status
  }, {
    where: {
      id: member.id
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });

};

dal.Member.get = (id) => {
  return db.Member.findOne({
    where: {
      id: id
    }
  }).then((data) => {
    if (data)
      return data.get({ plain: true });
  });
};

dal.Member.getByUserAndTeamId = (userId, teamId) => {
  return db.Member.findOne({
    where: {
      UserId: userId,
      TeamId: teamId
    }
  }).then((data) => {
    if (data)
      return data.get({ plain: true });
  });
};

dal.Member.remove = (id) => {
  return db.Member.destroy({
    where: {
      id: id
    }
  });
};

dal.Member.removeAll = (id) => {
  return db.Member.destroy({
    where: {
      TeamId: id
    }
  });
};

// Region: News

dal.News.create = (news) => {
  return db.News.create({
    title: news.title,
    body: news.body,
    GameId: news.GameId
  });
};

dal.News.update = (news) => {
  return db.News.update({
    title: news.title,
    body: news.body
  }, {
    where: {
      id: news.id
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });
};

dal.News.getById = (id) => {
  return db.News.findOne({
    where: {
      id: id
    }
  }).then((data) => {
    if (data)
      return data.get({ plain: true });
  });
};

dal.News.getByGameId_old = (gameId) => {
  return db.News.findAll({
    where: {
      [Op.or]: [
        { GameId: gameId },
        { GameId: null }
      ] 
    }
  }).then((data) => {
    var result = [];
    for (var i = 0; i < data.length; i++) {
      result.push(data[i].get({ plain: true }));
    }
    return result;
  })
};

dal.News.getByGameId = (gameId) => {
  return db.sequelize.query('SELECT * FROM "News" WHERE "GameId" = :gameId OR "GameId" is null', 
  { 
    model: db.News, 
    replacements: { gameId: gameId }, 
    type: db.sequelize.QueryTypes.SELECT
  }).then(news => {
    return news;
  });
}

dal.News.delete = (id) => {
  return db.News.destroy({
    where: {
      id: id
    }
  });
};

// Region: Team

dal.Team.create = (team) => {
  return db.Team.create({
    name: team.name,
    place: team.place,
    TournamentId: team.TournamentId,
    GameId: team.GameId
  });
};

dal.Team.update = (team) => {
  return db.Team.update({
    name: team.name,
    place: team.place
  }, {
    where: {
      id: team.id
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });
};

dal.Team.get = (id) => {
  return db.Team.findOne({
    where: {
      id: id
    },
    include: [{
      model: db.Member,
      attributes: ['id', 'inGameName', 'status']
    }, {
      model: db.Game,
      attributes: ['name']
    }]
  }).then((data) => {
    if (data)
      return data.get({ plain: true });
  });
};

dal.Team.adminGet = (id) => {
  return db.Team.findOne({
    where: {
      id: id
    },
    include: [
      {
        model: db.Member,
        include: [{
          model: db.User,
          include: [{
            model: db.Attendance
          }]
        }]
      },
      {
        model: db.Game
      }]
  }).then((data) => {
    if (data)
      return data.get({ plain: true });
  });
};

dal.Team.getAll = () => {
  return db.Team.findAll({
    include: [
      {
        model: db.Tournament,
      },
      {
        model: db.Game
      }
    ]
  }).then((data) => {
    var result = [];
    for (var i = 0; i < data.length; i++) {
      result.push(data[i].get({ plain: true }));
    }
    return result;
  });
};

dal.Team.getAllByUserId = (userId) => {
  return db.Member.findAll({
    where: {
      UserId: userId
    },
    include: [{
      model: db.Team,
      include: [{
        model: db.Tournament
      }, {
        model: db.Game
      }]
    }]
  });
};

dal.Team.adminGetAll = () => {
  // includes attendances for members
  return db.Team.findAll({
    include: [
      {
        model: db.Tournament,
      },
      {
        model: db.Game
      },
      {
        model: db.Member,
        include: [{
          model: db.User,
          include: [{
            model: db.Attendance
          }]
        }]
      }
    ]
  }).then((data) => {
    var result = [];
    for (var i = 0; i < data.length; i++) {
      result.push(data[i].get({ plain: true }));
    }
    return result;
  });
}

dal.Team.remove = (id) => {
  return db.Team.destroy({
    where: {
      id: id
    }
  });
};

dal.Team.getAllByTournamentId = (tournamentId) => {
  return db.Team.findAll({
    include: [{
      model: db.Tournament,
      where: {
        id: tournamentId
      }
    }, {
      model: db.Member
    }]
  }).then((data) => {
    var result = [];
    for (var i = 0; i < data.length; i++) {
      result.push(data[i].get({ plain: true }));
    }
    return result;
  });
}

dal.Team.getActiveByUserIdAndTournamentId = (userId, tournamentId) => {
  return db.Team.findOne({
    where: {
      TournamentId: tournamentId
    },
    include: [{
      model: db.Member,
      where: {
        UserId: userId
      }
    }]
  }).then((data) => {
    if (data) {
      return data.get({ plain: true });
    } else {
      return null;
    }
  })
};

dal.Team.setPlace = (teamId, place) => {
  return db.Team.update({
    place: place
  }, {
    where: {
      id: teamId
    }
  });
}

// Region: Tournament

dal.Tournament.create = (tournament) => {
  return db.Tournament.create({
    year: tournament.year,
    name: tournament.name,
    description: tournament.description,
    startDate: tournament.startDate,
    endDate: tournament.endDate,
    registrationStartDate: tournament.registrationStartDate,
    registrationEndDate: tournament.registrationEndDate,
  });
};

dal.Tournament.update = (tournament) => {
  return db.Tournament.update({
    year: tournament.year,
    name: tournament.name,
    description: tournament.description,
    startDate: tournament.startDate,
    endDate: tournament.endDate,
    registrationStartDate: tournament.registrationStartDate,
    registrationEndDate: tournament.registrationEndDate
  }, {
    where: {
      id: tournament.id
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });
};

dal.Tournament.get = (id) => {
  return db.Tournament.findOne({
    where: {
      id: id
    }
  }).then((data) => {
    if (data)
      return data.get({ plain: true });
  });
};

dal.Tournament.getActive = () => {
  return db.Tournament.findOne({
    where: {
      registrationStartDate: {
        $lt: new Date()
      },
      endDate: {
        $gt: new Date()
      }
    }
  }).then((data) => {
    if (data)
      return data.get({ plain: true });
  });
};

dal.Tournament.getAll = () => {
  return db.Tournament.findAll()
  .then((data) => {
    var result = [];
    for (var i = 0; i < data.length; i++) {
      result.push(data[i].get({ plain: true }));
    }
    return result;
  });
};

// Region: User

dal.User.create = (user) => {
  return db.User.create({
    name: user.name,
    passwordHash: user.passwordHash,
    ssn: user.ssn,
    phoneNumber: user.phoneNumber,
    email: user.email,
    username: user.username,
    role: user.role
  });
};

dal.User.update = (user) => {
  return db.User.update({
    name: user.name,
    phoneNumber: user.phoneNumber,
    email: user.email
  }, {
    where: {
      id: user.id
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });
};

dal.User.updatePassword = (hash, userId) => {
  return db.User.update({
    passwordHash: hash
  }, {
    where: {
      id: userId
    }
  });
}

dal.User.setRole = (user) => {
  return db.User.update({
    role: user.role
  }, {
    where: {
      id: user.id
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });
};

dal.User.setPassword = (user) => {
  return db.User.update({
    passwordHash: user.passwordHash,
    salt: user.salt
  }, {
    where: {
      username: user.username
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });
};

dal.User.setLanIp = (id, ip) => {
  return db.User.update({
    facebookId: ip,
  }, {
    where: {
      id: id
    },
    returning: true
  }).then((data) => {
    if (data[1].length > 0)
      return data[1][0].get({ plain: true });
  });
};

dal.User.getPrivateThings = (id) => {
  return db.User.findOne({
    where: {
      id: id
    }
  })
  .then((data) => {
    if (data)
      return data.get({ plain: true });
  });
}

dal.User.get = (id) => {
  return db.User.findOne({
    attributes: ['id', 'name', 'ssn', 'phoneNumber', 'email', 'username', 'role'],
    where: {
      id: id
    },
    include: [{
      model: db.Member,
    }, {
      model: db.Attendance
    }]
  }).then((data) => {
    if (data)
      return flattenAttendance(flattenMember(data.get({ plain: true })));
  });
};

dal.User.radiusRequest = (username) => {
  return db.User.findOne({
    attributes: ['id', 'username', 'passwordHash'],
    where: {
      username: username,
    },
    include: [{
      model: db.Attendance,
      include: [{
        model: db.Tournament,
        where: {
          year: new Date().getFullYear().toString()
        }
      }]
    }]
  }).then((data) => {
    if (data) {
      return data.get({ plain: true });
    } else {
      throw 'User not found.';
    }
  });
};

dal.User.getByUsername = (username) => {
  return db.User.findOne({
    attributes: ['id', 'name', 'ssn', 'phoneNumber', 'email', 'username', 'role', 'passwordHash'],
    where: {
      username: username
    },
    include: [{
      model: db.Attendance
    }]
  }).then((data) => {
    if (data)
      return flattenAttendance(data.get({ plain: true }));
    throw 'User not found.';
  });
};

dal.User.getByFacebookId = (facebookId) => {
  return db.User.findOne({
    attributes: ['id', 'name', 'ssn', 'phoneNumber', 'email', 'username', 'role'],
    where: {
      facebookId: facebookId
    },
    include: [{
      model: db.Attendance
    }]
  }).then((data) => {
    if (data)
      return flattenAttendance(data.get({ plain: true }));
  });
};

dal.User.getByEmail = (email) => {
  return db.User.findAll({
    attributes: ['id', 'name', 'ssn', 'phoneNumber', 'email', 'username', 'role', 'passwordHash'],
    where: {
      email: {
        $iLike: email
      }
    }
  }).then((data) => {
    let result = [];
    for (let i = 0; i < data.length; i++) {
      result.push(data[i].get({ plain: true }));
    }
    return result;
  });
};

dal.User.getAll = () => {
  return db.User.findAll({
    attributes: ['id', 'name', 'ssn', 'phoneNumber', 'email', 'username', 'role', 'facebookId'],
    include: [{
      model: db.Member,
      include: [{
        model: db.Team,
        include: [{
          model: db.Game
        }]
      }]
    }, {
      model: db.Attendance
    }]
  }).then((data) => {
    let result = [];
    for (let i = 0; i < data.length; i++) {
      result.push(flattenMember(flattenAttendance(data[i].get({ plain: true }))));
    }
    return result;
  });
};


// Region: Voucher

dal.Voucher.getNext = () => {
  return new Promise(function(resolve, reject) {
    var code;
    db.Voucher.findOne().then((data) => {
      if (data)
        return data.get({ plain: true });
    }).then((voucher) => {
      code = voucher.code;
      return db.Voucher.destroy({
        where: {
          id: voucher.id
        }
      });
    }).then(() => {
      resolve(code);
    });
  });
};


// Region: ResetPassword

dal.ResetPassword.get = (uuid) => {
  return db.ResetPassword.findOne({
    attributes: ['uuid', 'expires', 'active', 'UserId'],
    where: {
      uuid: uuid
    }
  }).then((data) => {
    if (data)
      return data.get({ plain: true });
  });
};

dal.ResetPassword.create = (resetPassword, user) => {
  return db.ResetPassword.create({
    expires: resetPassword.expires,
    uuid: resetPassword.uuid,
    active: true,
    UserId: user.id
  });
};

dal.ResetPassword.deactivate = (uuid) => {
  return db.ResetPassword.update({
    active: false
  }, {
    where: {
      uuid: uuid
    }
  });
};

module.exports = dal;


function flattenMember (user) {
  return user;
}

function flattenAttendance (user) {
  return user;
}

function getAttendance (att) {
  if (att.length === 0) {
    return 'none';
  } else if (att[0].payed === true) {
    return 'payed';
  } else if (att[0].payed === false) {
    return 'pending';
  }
}
