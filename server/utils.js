const utils = {};
const config = require('./config'); // get our config file
const nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport({
  host: 'mail.tviund.com',
  port: 465,
  secure: true,
  auth: {
    user: 'noreply@tviund.com',
    pass: 'B1nary1337'
  }
});

utils.Mailer = {
  send: async (email, subject, body) => {
    let info = await transporter.sendMail({
      from: 'noreply@tviund.com',
      to: email,
      subject: subject,
      html: body
    });
    return info.messageId;
  }
};

module.exports = utils;
