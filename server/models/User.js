'use strict';

module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    passwordHash: {
      type: DataTypes.STRING,
      allowNull: false
    },
    ssn: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    phoneNumber: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    facebookId: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: false
    },
    role: {
      type: DataTypes.ENUM,
      values: ['admin', 'shop', 'checkin', 'council', 'public', 'superadmin'],
      allowNull: false
    }
  }, {
    classMethods: {
      associate: function (models) {
        User.hasMany(models.Attendance, {
          foreignKey: {
            allowNull: false
          }
        });
        User.hasMany(models.Member, {
          foreignKey: {
            allowNull: false
          }
        });
        User.hasMany(models.ResetPassword, {
          foreignKey: {
            allowNull: false
          }
        });
      }
    }
  });
  return User;
};
