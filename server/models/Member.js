'use strict';

module.exports = (sequelize, DataTypes) => {
  var Member = sequelize.define('Member', {
    status: {
      type: DataTypes.ENUM,
      values: ['confirmed', 'pending', 'captain'],
      allowNull: false
    },
    inGameName: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    classMethods: {
      associate: function (models) {
        Member.belongsTo(models.User, {
          foreignKey: {
            allowNull: false
          }
        });
        Member.belongsTo(models.Team, {
          foreignKey: {
            allowNull: false
          }
        });
      }
    }
  });
  return Member;
};
