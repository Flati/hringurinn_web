'use strict';

module.exports = (sequelize, DataTypes) => {
  var Tournament = sequelize.define('Tournament', {
    year: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    startDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    endDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    registrationStartDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    registrationEndDate: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    classMethods: {
      associate: function (models) {
        Tournament.hasMany(models.Team, {
          foreignKey: {
            allowNull: false
          }
        });
        Tournament.hasMany(models.Attendance, {
          foreignKey: {
            allowNull: false
          }
        });
      }
    }
  });
  return Tournament;
};
