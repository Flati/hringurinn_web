'use strict';

module.exports = (sequelize, DataTypes) => {
  var Voucher = sequelize.define('Voucher', {
    code: {
      type: DataTypes.STRING,
      allowNull: false
    }
  });
  return Voucher;
};
