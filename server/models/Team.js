'use strict';

module.exports = (sequelize, DataTypes) => {
  var Team = sequelize.define('Team', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    place: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function (models) {
        Team.belongsTo(models.Tournament, {
          foreignKey: {
            allowNull: false
          }
        });
        Team.hasMany(models.Member, {
          foreignKey: {
            allowNull: false
          }
        });
        Team.belongsTo(models.Game, {
          foreignKey: {
            allowNull: false
          }
        });
      }
    }
  });
  return Team;
};
