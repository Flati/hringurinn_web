'use strict';

module.exports = (sequelize, DataTypes) => {
  var Attendance = sequelize.define('Attendance', {
    payed: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    payerSSN: {
      type: DataTypes.STRING,
      allowNull: false
    },
    voucher: DataTypes.STRING,
    amount: DataTypes.INTEGER,
    parentName: DataTypes.STRING,
    parentSSN: DataTypes.STRING,
    parentPhone: DataTypes.STRING
  }, {
    classMethods: {
      associate: function (models) {
        Attendance.belongsTo(models.Tournament, {
          foreignKey: {
            allowNull: false
          }
        });
        Attendance.belongsTo(models.User, {
          foreignKey: {
            allowNull: false
          }
        });
      }
    }
  });
  return Attendance;
};
