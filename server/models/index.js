'use strict';

const Sequelize = require('sequelize');
const sequelize = new Sequelize('hr', 'hr', 'fokkfeis', {
      dialect: "postgres",
      port:    5432,
      logging: false
    });

sequelize
  .authenticate()
  .then(function(err) {
    console.log('Connection has been established successfully.');
  }, function (err) {
    console.log('Unable to connect to the database:', err);
  });

// Setup db
const db = {
  sequelize: sequelize,
  Sequelize: Sequelize,
};
// Add models
db.Attendance = sequelize.import(__dirname + "/Attendance.js");
db.Game = sequelize.import(__dirname + "/Game.js");
db.Member = sequelize.import(__dirname + "/Member.js");
db.News = sequelize.import(__dirname + "/News.js");
db.ResetPassword = sequelize.import(__dirname + '/ResetPassword.js');
db.Team = sequelize.import(__dirname + "/Team.js");
db.Tournament = sequelize.import(__dirname + "/Tournament.js");
db.User = sequelize.import(__dirname + "/User.js");
db.Voucher = sequelize.import(__dirname + "/Voucher.js");

// Associate foreign keys
db.Attendance.associate(db);
db.Game.associate(db);
db.Member.associate(db);
db.News.associate(db);
db.ResetPassword.associate(db);
db.Team.associate(db);
db.Tournament.associate(db);
db.User.associate(db);

sequelize
  .sync({ force: false })
  .then(function(err) {
    console.log('It worked!');
    //insertInitialData();
  }, function (err) {
    console.log('An error occurred while creating the table:', err);
  });

function insertInitialData() {
  db.Tournament.create({
    year: 2016,
    name: 'HRingurinn 2016',
    description: 'Stærstu tölvuleikar landsins.',
    startDate: new Date(2016, 7, 5, 18, 0, 0),
    endDate: new Date(2016, 7, 7, 17, 0, 0),
    registrationStartDate: new Date(2016, 5, 17, 17, 6, 16),
    registrationEndDate: new Date(2016, 7, 3, 0, 0, 0)
  }).then(() => {
    return db.Game.create({
      name: 'CS:GO',
      description: 'Counter-Strike: Global Offensive',
      active: true,
      size: 5
    });
  }).then(() => {
    return db.Game.create({
      name: 'LoL',
      description: 'League of Legends',
      active: true,
      size: 5
    });
  }).then(() => {
    return db.Game.create({
      name: 'Hearthstone',
      description: 'Hearthstone: Heroes of Warcraft',
      active: true,
      size: 1
    });
  }).then(() => {
    return db.Game.create({
      name: 'Overwatch',
      description: 'Overwatch',
      active: true,
      size: 6
    });
  }).then(() => {
    return db.Game.create({
      name: 'Rocket League',
      description: 'Rocket League',
      active: true,
      size: 3
    });
  });
}

function insertTestData() {
  // Insert test data one at a time
  const data = {};
  db.Tournament.create({
    year: 2016,
    name: 'HRingurinn',
    description: 'Lan mót',
    startDate: new Date(2016, 6, 1),
    endDate: new Date(2016, 7, 1),
    registrationStartDate: new Date(2016, 5, 1),
    registrationEndDate: new Date(2016, 6, 1),
  })
  .then((tournament) => {
    data.tournament = tournament;
    return db.Game.create({
      name: 'CS:GO',
      description: 'Skotleikur of stuff',
      active: true,
      size: 5
    });
  })
  .then((game) => {
    data.game2 = game;
    return db.Game.create({
      name: 'LoL',
      description: 'MOBA',
      active: true,
      size: 5
    });
  })
  .then((game) => {
    data.game = game;
    return db.Team.create({
      name: 'TeamName',
      TournamentId: data.tournament.id,
      GameId: game.id,
    });
  })
  .then((team) => {
    data.team = team;
    return db.News.create({
      title: "Breaking news",
      time: new Date(),
      body: "Lorum ipsum",
      GameId: data.Game,
    });
  })
  .then((news) => {
    data.news = news;
    return db.User.create({
      name: 'Katur',
      passwordHash: '$2a$10$upAOK1Cbitjv24qO7IYU2e6k1wbucx.PmsMShANpqyjQoU5YGPmQm',
      ssn: '1234567890',
      phoneNumber: '112',
      email: 'fake@email.internet',
      username: 'Katur',
      facebookId: '65468651651684684',
      role: 'admin',
    });
  })
  .then((user) => {
    data.user = user;
    return db.Attendance.create({
      payed: false,
      payerSSN: "1234567890",
      timePayed: new Date(),
      voucher: "asdJOQWDqw)877=",
      TournamentId: data.tournament.id,
      UserId: data.user.id,
    });
  })
  .then((attendance) => {
    data.attendance = attendance;
    return db.Member.create({
      status: 'pending',
      inGameName: "Katur",
      TeamId: data.team.id,
      UserId: data.user.id,
    });
  });
}


module.exports = db;
