'use strict';

module.exports = (sequelize, DataTypes) => {
  var News = sequelize.define('News', {
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    body: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    author: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    classMethods: {
      associate: function (models) {
        News.belongsTo(models.Game, {
          foreignKey: {
            allowNull: true
          }
        });
      }
    }
  });
  return News;
};
