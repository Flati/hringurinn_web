'use strict';

module.exports = (sequelize, DataTypes) => {
  var ResetPassword = sequelize.define('ResetPassword', {
    expires: {
      type: DataTypes.DATE,
      allowNull: false
    },
    uuid: {
      type: DataTypes.UUID,
      allowNull: false,
      unique: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    classMethods: {
      associate: function (models) {
        ResetPassword.belongsTo(models.User, {
          foreignKey: {
            allowNull: false
          }
        });
      }
    }
  });
  return ResetPassword;
};
