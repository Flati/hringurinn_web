'use strict';

module.exports = (sequelize, DataTypes) => {
  var Game = sequelize.define('Game', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    twitchUrl: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    size: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    classMethods: {
      associate: function (models) {
        Game.hasMany(models.News, {
          foreignKey: {
            allowNull: true
          }
        });
        Game.hasMany(models.Team, {
          foreignKey: {
            allowNull: false
          }
        });
      }
    }
  });
  return Game;
};
