"use strict";

const express   = require('express');
const cors      = require('cors');
const helmet    = require('helmet');
const morgan    = require('morgan');
const greenlock = require('greenlock-express');
const http 	    = require('http');
const https	    = require('https');
const redHttps	= require('redirect-https');

const api       = require('./api.js');
const RadiusServer    = require('./radius.js');

const app = express();

app.use(helmet()); // security middleware

const corsWhitelist = ['https://hringurinn.net', 'http://localhost'];
const corsOptions = {
  origin: function(origin, callback) {
    callback(null, true); // allow everything
    // if (corsWhitelist.indexOf(origin) !== -1 || !origin) {
    // } else {
    //   //callback(new Error('Not allowed by CORS'));
    //   callback(null, true);
    // }
  }
}
app.use(cors(corsOptions));
app.use(morgan('dev')); // log requests to the console
app.use('/api', api);

const rServer = new RadiusServer({ port: 1645, secret: "fokkfeisRADIUS" });
rServer.start();

const httpPort = 8080;
const httpsPort = 4433;

greenlock.init(function() {
    let pkg = require('./package.json');
    return {
    	greenlock: require("@root/greenlock").create({
                // name & version for ACME client user agent
                packageAgent: pkg.name + "/" + pkg.version,

                // contact for security and critical bug notices
                maintainerEmail: pkg.author,

                // where to find .greenlockrc and set default paths
                packageRoot: __dirname
            }),

            // whether or not to run at cloudscale
            cluster: false
        };
    })
    .ready(function(glx) {
        // Serves on 80 and 443
        // Get's SSL certificates magically!
        glx.serveApp(app);
    });
