'use strict';

const express = require('express');
const server = express();
const app = express.Router();
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('./config'); // get our config file
const dal = require('./dal');
const bcrypt = require('bcryptjs');

const usersCtrl = require('./users');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Routing
app.use('/users', usersCtrl);

const handleError = (err, res) => {
  if (err.name === 'ValidationError') {
    return res.status(400).send(err);
  } else if (err.name === 'BadRequestError') {
    return res.status(401).send(err);
  } else if (err.name === 'NotFoundError') {
    return res.status(404).send(err);
  } else if (err.name === 'PreconditionFailedError') {
    return res.status(412).send(err);
  }
  return res.status(500).send('Internal server error');
};

app.get('/', (req, res) => {
  return res.status(200).send("Hello HRingurinn!");
});

// route middleware to verify a token
// Everything below this is protected and needs a token to work
app.use((req, res, next) => {
  // check header or url parameters or post parameters for token
  let token = req.get('Authorization') || req.body.token || req.query.token;
  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, config.jwtSecret, (err, decoded) => {
      if (err) {
        return res.status(401).json({success: false, message: 'Failed to authenticate token.'});
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });
  } else {
    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });
  }
});

app.get('/user', (req, res) => {
  return dal.User.get(req.decoded.userId).then((user) => {
    return res.status(200).send(user);
  });
});

app.get('/user/team/active', (req, res) => {
  return dal.Tournament.getActive()
  .then((tournament) => {
    return dal.Team.getActiveByUserIdAndTournamentId(req.decoded.userId, tournament.id)
  })
  .then((team) => {
    return res.status(200).send(team);
  })
  .catch((err) => {
    console.error(err);
    return res.sendStatus(404);
  });
});

app.get('/user/teams', (req, res) => {
  return dal.Team.getAllByUserId(req.decoded.userId).then((teams) => {
    return res.status(200).send(teams);
  }).catch(err => this.handleError(err, res));
});

app.get('/users', (req, res) => {
  return dal.User.get(req.decoded.userId)
  .then((user) => {
    if (user && user.role !== 'public') {
      return dal.User.getAll();
    } else {
      return res.status(403);
    }
  })
  .then((users) => {
    return res.status(200).send(users);
  })
  .catch((err) => {
    console.log(err);
    return res.status(401).send(err);
  });
});

app.put('/users/:id', (req, res) => {
  let model = req.body;
  if (+req.params.id !== +req.decoded.userId) {
    return res.status(400).send({ message: 'Cannot edit other users than yourself' });
  }
  return dal.User.update({
    name: model.name,
    phoneNumber: model.phoneNumber,
    email: model.email,
    id: req.decoded.userId
  }).then((user) => {
    return res.status(200).send({
      name: user.name,
      phoneNumber: user.phoneNumber,
      email: user.email
    });
  })
  .catch((err) => {
    console.log(err);
    return res.status(400).send(err);
  });
});

app.post('/user/password', (req, res) => {
  let model = req.body;
  return dal.User.getPrivateThings(req.decoded.userId)
  .then((user) => {
    console.log('user with id : ' + req.decoded.userId + ' wants to change password');
    if(user && bcrypt.compareSync(model.old, user.passwordHash)) {
      console.log('user with id : ' + req.decoded.userId + ' is changing password, old password correct');
      // change password
      const passwordHash = bcrypt.hashSync(model.newPass, 8);
      return dal.User.updatePassword(passwordHash, req.decoded.userId)
      .then(() => {
        console.log('user with id : ' + req.decoded.userId + ' changed password');
        return res.sendStatus(204);
      })
      .catch((e) => {
        console.log(e);
        return res.send(e).status(400);
      });
    } else {
      console.log('user with id : ' + req.decoded.userId + ' failed in changing password, old password incorrect');
      return res.status(403).send("WRONG_PASSWORD");
    }
  });
});

app.get('/games', (req, res) => {
  return dal.Game.getAll().then((games) => {
    return res.status(200).send(games);
  });
});

app.get('/tournaments', (req, res) => {
  return dal.Tournament.getAll().then((tournaments) => {
    return res.status(200).send(tournaments);
  })
  .catch((err) => {
    console.log(err);
    return res.sendStatus(404);
  });
})

app.get('/tournaments/active', (req, res) => {
  dal.Tournament.getActive()
  .then((val) => {
    return res.status(200).send(val);
  });
});

app.get('/tournaments/:tournamentId/attendance', (req, res) => {
  return dal.Attendance.getByUserIdAndTournamentId(+req.decoded.userId, +req.params.id)
  .then((val) => {
    return res.status(200).send(val);
  })
  .catch((err) => {
    return res.sendStatus(404);
  })
});

app.get('/teams', (req, res) => {
  dal.Tournament.getActive()
  .then((val) => {
    // returns Tournament object
    return dal.Team.getAllByTournamentId(val.id);
  })
  .then((teams) => {
    return res.status(200).send(teams);
  })
  .catch((err) => {
    console.log(err);
    return res.sendStatus(400);
  });
});

app.put('/admin/game/:id', (req, res) => {
  return dal.User.get(req.decoded.userId)
  .then((user) => {
    if (user && user.role === 'superadmin') {
      let model = req.body;
      if (model.id !== +req.params.id) {
        return res.sendStatus(400);
      }
      return dal.Game.update(model)
      .then((team) => {
        return res.status(200).send(team);
      })
      .catch((err) => {
        console.log(err);
        return res.sendStatus(500);
      });
    }
  })
  .catch((err) => {
    console.log(err);
    return res.sendStatus(403);
  })
});

app.get('/admin/teams', adminRoute, (req, res) => {
  return dal.Team.adminGetAll()
  .then((teams) => {
    return res.status(200).send(teams);
  })
  .catch(err => this.handleError(err, res));
});

app.get('/admin/teams/:id', adminRoute, (req, res) => {
  return dal.Team.adminGet(req.params.id)
  .then((team) => {
    return res.status(200).send(team)
  })
  .catch(err => this.handleError(err, res));
});

app.put('/admin/teams/:id', adminRoute, (req, res) => {
  if (!req.body || !req.body.place) {
    return res.status(412).send('Missing parameter: place');
  }
  return dal.Team.setPlace(req.params.id, req.body.place)
  .then(() => {
    return res.sendStatus(200);
  }).catch((err) => this.handleError(err, res));
});

app.get('/teams/:id', (req, res) => {
  return dal.Team.get(req.params.id)
  .then((team) => {
    return res.status(200).send(team);
  })
  .catch(err => this.handleError(err, res));
});

app.delete('/teams/:id', (req, res) => {
  return dal.Member.getByUserAndTeamId(req.decoded.userId, req.params.id)
  .then((member) => {
    if (!member) {
      return res.sendStatus(400);
    } else if (member.status !== 'captain') {
      return res.sendStatus(403);
    }
    return dal.Member.removeAll(req.params.id).then(() => {
      return dal.Team.remove(req.params.id);
    }).then(() => {
      return res.sendStatus(200);
    });
  })
  .catch((err) => {
    console.log(err);
    return res.sendStatus(400);
  });
});

app.delete('/admin/teams/:id', (req, res) => {
  return dal.User.get(req.decoded.userId)
  .then((user) => {
    if (user && user.role === 'superadmin') {
      // only superadmin should be able to delete other teams
      return dal.Member.removeAll(req.params.id).then(() => {
        return dal.Team.remove(req.params.id);
      }).then(() => {
        return res.sendStatus(200);
      });
    }
  })
  .catch((err) => {
    console.log(err);
    return res.sendStatus(403);
  });
});

app.post('/teams/', (req, res) => {
  var model = req.body;
  // get newest tournament
  return dal.Tournament.getActive()
  .then((tournament) => {
    return dal.Team.create({
      name: model.name,
      GameId: model.game,
      TournamentId: tournament.id
    });
  })
  .then((team) => {
    return dal.Member.create({
      status: 'captain',
      inGameName: model.inGameName,
      UserId: req.decoded.userId,
      TeamId: team.id
    });
  }).then((member) => {
    return res.sendStatus(201);
    // res.status..send failing for some reason
    // return res.status(200).send(member.TeamId);
  })
  .catch((err) => {
    console.log('failed creating team ------------------------');
    console.log(err.message);
    console.log(err);
    return res.status(400).send(err);
  })
});


app.post('/teams/:teamId/members', (req, res) => {
  var model = req.body;
  if (!model.inGameName)
    return res.sendStatus(400);
  return dal.Member.create({
    status: 'pending',
    inGameName: model.inGameName,
    TeamId: req.params.teamId,
    UserId: req.decoded.userId
  }).then((member) => {
    return res.status(200).send(member);
  })
  .catch((err) => {
    console.log(err);
    return res.status(400).send(err);
  })
});

app.delete('/teams/:teamId/members/:memberId', validateAllowedToEditMember, (req, res) => {
  dal.Member.remove(req.params.memberId)
    .then((deletedMember) => {
      if (!deletedMember)
        return res.sendStatus(500);
      return res.sendStatus(200);
    });
});


app.put('/teams/:teamId/members/:memberId', validateAllowedToEditMember, (req, res) => {
  var model = req.body;
  var dbReq = {
    id: req.params.memberId,
    status: model.status,
    inGameName: model.inGameName
  };

  if (!req.decoded.isCaptain)
    delete dbReq['status'];

  dal.Member.update(dbReq)
    .then((member) => {
      if (!member)
        return res.sendStatus(500);
      return res.status(200).send(member);
    });
});

app.post('/teams/:id/status', (req, res) => {
  var model = req.body;
  return dal.Member.getByUserAndTeamId(req.decoded.userId, req.params.id)
  .then((member) => {
    if (!member) {
      return res.sendStatus(400);
    } else if (member.status !== 'captain') {
      return res.sendStatus(403);
    }
    if (model.accept) {
      return dal.Member.setStatus({
        id: model.id,
        status: 'confirmed'
      });
    } else {
      return dal.Member.remove(model.id);
    }
  }).then(() => {
    res.sendStatus(200);
  });
});

app.post('/payment', (req, res) => {
  var model = req.body;
  if (model.ssn) {
    return dal.Tournament.getActive()
    .then((tournament) => {
      return dal.Attendance.create({
        payed: false,
        payerSSN: model.ssn,
        UserId: req.decoded.userId,
        TournamentId: tournament.id
      })
    })
    .then((data) => {
      return res.status(200).send(data);
    });
  } else if (model.delete) {
    return dal.Tournament.getActive()
    .then((tournament) => {
      return dal.Attendance.getByUserIdAndTournamentId(req.decoded.userId, tournament.id)
    })
    .then((attendance) => {
      if (attendance) {
        return dal.Attendance.remove(attendance.id)
        .then(() => {
          return res.sendStatus(204);
        })
        .catch((err) => {
          console.log('Attendance remove error');
          console.log(err);
          return res.sendStatus(500);
        });
      } else {
        console.log('user with id : ' + req.decoded.userId + ' tried to delete a non-existant Attendance');
        return res.sendStatus(404);
      }
    })
    .catch((err) => {
      console.log(err);
      return res.sendStatus(500);
    })
  } else {
    return res.sendStatus(412);
  }
});

app.post('/adminUpdate', (req, res) => {
  let model = req.body;
  /** model attributes :
    id: number,
    parentName: string,
    parentSSN: string,
    parentPhone: string,
    payment: string,
    role: Role,
    password: string,
    voucher: string,
    amount: number | null
   */
  return dal.User.get(req.decoded.userId)
  .then((user) => {
    if (user && user.role !== 'public') {
      return dal.User.setRole(model)
      .then((data) => {
        return dal.User.get(model.id);
      }).then((data) => {
        if (data.Attendances && data.Attendances.length && data.Attendances.length > 0) {
          // SHIT FIX!!! PLEASE DO THIS PROPERLY WHEN YOU SEE THIS FUCKING COMMENT!
          let idx = data.Attendances.findIndex(x => x.TournamentId === 2);
          if (idx === -1) {
            // create new
            return dal.Attendance.create({
              payed: model.payment === 'payed',
              payerSSN: '',
              UserId: model.id,
              TournamentId: 2, // fix this shit
              parentName: model.parentName,
              parentSSN: model.parentSSN,
              parentPhone: model.parentPhone,
              voucher: model.voucher,
              amount: model.amount
            });
          } else {
            if (model.payment === 'none') {
              return dal.Attendance.remove(data.Attendances[idx].id);
            } else {
              return dal.Attendance.updatePayed({
                id: data.Attendances[idx].id,
                payed: model.payment === 'payed',
                parentName: model.parentName,
                parentSSN: model.parentSSN,
                parentPhone: model.parentPhone,
                voucher: model.voucher,
                amount: model.amount
              });
            }
          }
        } else {
          if (model.payment === 'none') {
            // return dal.Attendance.get(data.Attendance.id); // if data.Attendance is undefined, why should data.Attendance.id not be??
            return data;
          } else {
            return dal.Tournament.getActive()
            .then((tournament) => {
              return dal.Attendance.create({
                payed: model.payment === 'payed',
                payerSSN: '',
                UserId: model.id,
                TournamentId: tournament.id,
                parentName: model.parentName,
                parentSSN: model.parentSSN,
                parentPhone: model.parentPhone,
                voucher: model.voucher,
                amount: model.amount
              });
            })
          }
        }
      }).then((data) => {
        if (model.password !== null
            && model.password !== ''
            && model.password.length
            && model.password.length > 0
            && user.role === 'superadmin') {
          // change user password if allowed
          try {
            const passwordHash = bcrypt.hashSync(model.password, 8);
            return dal.User.updatePassword(passwordHash, model.id)
            .then(() => {
              return res.sendStatus(200);
            })
            .catch((e) => {
              console.log(e);
              return res.send(e).status(400);
            });
          } catch (e) {
            console.log(e);
            return res.send(e).status(500);
          }
        } else {
          return res.sendStatus(200);
        }
      });
    } else {
      return res.sendStatus(403);
    }
  });
});

app.post('/getVoucher', (req, res) => {
  let model = req.body;
  return dal.User.get(model.id).then((user) => {
    // TODO: FIX for new attendances model
    if (!user.Attendance) {
      return res.status(403).send('Notandi er ekki með greiðslustöðu.');
    } else if (user.Attendance.voucher) {
      return res.status(200).send(VoucherHTML(user.name, user.Attendance.voucher));
    }
    return dal.Voucher.getNext().then((code) => {
      return dal.Attendance.setVoucher({
        id: user.Attendance.id,
        voucher: code
      }).then(() => {
        return res.status(200).send(VoucherHTML(user.name, code));
      });
    });
  });
});

app.post('/news', (req, res) => {
  const model = req.body;
  return dal.News.create({ title: model.title, body: model.body, GameId: model.GameId })
  .then(() => {
    return res.sendStatus(201);
  })
  .catch((err) => {
    console.log(err);
    return res.sendStatus(400);
  });
});

app.get('/news', (req, res) => {
  return dal.News.getByGameId(null)
  .then((news) => {
    return res.status(200).send(news);
  })
  .catch((err) => {
    console.log(err);
    return res.sendStatus(500);
  });
});

app.get('/news/:gameId', (req, res) => {
  return dal.News.getByGameId(req.params.gameId)
  .then((news) => {
    return res.status(200).send(news);
  })
  .catch((err) => {
    console.log(err);
    return res.sendStatus(500);
  });
});

app.put('/news/:id', (req, res) => {
  const model = req.body;
  if (!model.id) {
    return res.sendStatus(412);
  }
  return dal.News.update(model)
  .then((news) => {
    return res.status(200).send(news);
  })
  .catch((err) => {
    console.log(err);
    return res.sendStatus(500);
  })
});

app.delete('/news/:id', (req, res) => {
  if (!req.params.id) {
    return res.sendStatus(412);
  }
  return dal.News.delete(req.params.id)
  .then(() => {
    return res.sendStatus(204);
  })
  .catch((err) => {
    console.log(err);
    return res.sendStatus(400);
  })
});

function validateAllowedToEditMember(req, res, next) {
  let memberId = req.params.memberId;
  let teamId = req.params.teamId;
  let userId = req.decoded.userId;
  req.decoded.isCaptain = false;
  if (!memberId || !teamId || !userId)
    return res.sendStatus(400);
  dal.Member.get(memberId)
  .then((member) => {
    if (!member)
      return res.sendStatus(404);
    if (member.TeamId == teamId && member.UserId == userId)
      return next();
    return dal.Member.getByUserAndTeamId(userId, teamId).then((member) => {
      if (!member)
        return res.sendStatus(400);

      if (member.status === 'captain') {
        req.decoded.isCaptain = true;
        next();
      }
      else
        return res.sendStatus(403);
    });
  });
}

function adminRoute(req, res, next) {
  let userId = req.decoded.userId;
  dal.User.getPrivateThings(userId)
  .then((user) => {
    if (user && user.role && user.role !== 'public') {
      next();
    } else {
      return res.sendStatus(403);
    }
  })
}


function VoucherHTML(name, code) {
  return '<html><head><title>HRingurinn Voucher</title></head>' +
         '<body style="padding: 100px 50px;"><div style="float:left;width:300px;"><strong>' +
         name +
         '</strong></div><div style="float:left;"><strong>' +
         code +
         '</strong></div></body></html>';
}

module.exports = app;
